package net.scholtes.banditdust;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;

import net.scholtes.banditdust.Events.DropEvent;

public class Main extends JavaPlugin {

	public static Main instance;
	public static WorldGuardPlugin wg;

	public void onEnable() {

		instance = this;

		getCommand("setpickaxe").setExecutor(new Commands());
		getCommand("removepickaxe").setExecutor(new Commands());
		getCommand("settag").setExecutor(new Commands());
		getCommand("removetag").setExecutor(new Commands());
		getCommand("setarmor").setExecutor(new Commands());
		getCommand("removearmor").setExecutor(new Commands());
		getCommand("setscroll").setExecutor(new Commands());
		getCommand("removescroll").setExecutor(new Commands());
		getCommand("setpet").setExecutor(new Commands());
		getCommand("removepet").setExecutor(new Commands());
		getCommand("givedust").setExecutor(new Commands());

		getServer().getPluginManager().registerEvents(new DropEvent(), this);

		loadConfig();

		wg = getWorldGuard();

	}

	public void onDisable() {

	}

	public static Main getInstance() {
		return instance;
	}

	public static double round(double value, int decimals) {
		double p = Math.pow(10, decimals);
		return Math.round(value * p) / p;
	}

	public WorldGuardPlugin getWorldGuard() {
		Plugin plugin = getServer().getPluginManager().getPlugin("WorldGuard");

		if (plugin == null || !(plugin instanceof WorldGuardPlugin)) {
			return null;
		}

		return (WorldGuardPlugin) plugin;
	}

	public void loadConfig() {
		getConfig().options().copyDefaults(true);
		saveConfig();
	}

}